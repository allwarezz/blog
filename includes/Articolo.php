<?php
namespace DataHandling;

class Articolo extends FormHandle
{

    protected static function sanitize($fields)
    {
        $errors = array();
        $fields['titolo'] = self::cleanInput($fields['titolo']);
        $fields['testo'] = self::cleanInput($fields['testo']);

        $fields['immagine'] = self::cleanInput($fields['immagine']);

        if ($fields['immagine'] !== '') {
            if (!self::isUrlValid($fields['immagine'])) {
                return false;
            }
        }

        return $fields;
    }

    public static function insertData($form_data, $loggedInUserId)
    {
        $fields = array(
            'titolo' => $form_data['titolo'],
            'testo' => $form_data['testo'],
            'immagine' => $form_data['immagine'],
        );
        $fields['bozza'] = (isset($form_data['bozza']) && $form_data['bozza'] === 'on') ? 1 : 0;
        $fields['pubblicato'] = (isset($form_data['pubblicato']) && $form_data['pubblicato'] === 'on') ? 1 : 0;
        if ($fields['titolo'] === '' || $fields['testo'] === '') {
            header('Location: http://localhost/blog/insmod-articolo.php?stato=ko'
                . '&message=Titolo e Testo non possono essere lasciati vuoti');
            exit;
        }
        $fields = self::sanitize($fields);

        if (!$fields) {
            $titolo = $form_data['titolo'];
            $testo = $form_data['testo'];
            header('Location: http://localhost/blog/insmod-articolo.php?stato=ko'
                . '&message=URL Immagine Non Valido&titolo=' . $titolo . '&testo=' . $testo);
            exit;
        }

        if ($fields) {
            $mysqli = \DBHandle\getConnection();

            $loggedInUserId = intval($loggedInUserId);
            $query = $mysqli->prepare('INSERT INTO articoli(id_utente, titolo, testo, immagine, bozza, pubblicato)
            VALUES (?, ?, ?, ?, ?, ?)');
            $img = ($fields['immagine'] !== '') ? $fields['immagine'] : null;
            $query->bind_param(
                'isssii',
                $loggedInUserId,
                $fields['titolo'],
                $fields['testo'],
                $img,
                $fields['bozza'],
                $fields['pubblicato']
            );
            $query->execute();

            if ($query->affected_rows === 0) {
                error_log('Errore MySQL: ' . $query->error_list[0]['error']);
                header('Location: http://localhost/blog/insmod-articolo.php?stato=ko'
                    . '&message=Ops, problema nell\'inserimento dell\'articolo');
                exit;
            }

            $query->close();

            header('Location: http://localhost/blog/insmod-articolo.php?stato=ok'
                . '&message=Articolo Inserito Correttamente');
            exit;
        }
    }

    public static function selectData($args = null)
    {
        $mysqli = \DBHandle\getConnection();

        if (!isset($args['id_articolo']) && !isset($args['id_utente'])) {
            $res = $mysqli->query('SELECT articoli.id, utenti.username as autore, articoli.titolo, articoli.testo,
            articoli.immagine, articoli.creazione as dataArticolo
            FROM articoli JOIN utenti ON utenti.id = articoli.id_utente
            WHERE pubblicato = 1 ORDER BY creazione DESC');
        } elseif (isset($args['id_articolo']) && !isset($args['action'])) {
            $args['id_articolo'] = intval($args['id_articolo']);
            $query = $mysqli->prepare('SELECT articoli.id, utenti.id as id_utente, utenti.username as autore,
            articoli.titolo, articoli.testo, articoli.creazione as dataArticolo, articoli.immagine FROM articoli
            JOIN utenti ON utenti.id = articoli.id_utente WHERE articoli.id = ?');

            $query->bind_param('i', $args['id_articolo']);
            $query->execute();
            $res = $query->get_result();
            $query->close();
            if ($res->num_rows) {
                $res_c = \DataHandling\Commento::selectData($args);
            }
        } elseif (!isset($args['id_articolo']) && isset($args['id_utente'])) {
            $query = $mysqli->prepare('SELECT articoli.id, utenti.username as autore, articoli.titolo,
            articoli.creazione as dataArticolo, articoli.bozza, articoli.pubblicato
            FROM articoli JOIN utenti ON utenti.id = articoli.id_utente
            WHERE articoli.id_utente = ? ORDER BY creazione DESC');
            $query->bind_param('i', $args['id_utente']);
            $query->execute();
            $res = $query->get_result();
            $query->close();
        } elseif (isset($args['id_articolo']) && isset($args['action']) && $args['action'] === 'update') {
            $query = $mysqli->prepare('SELECT * FROM articoli WHERE id = ? AND id_utente = ?');
            $query->bind_param('ii', $args['id_articolo'], $args['id_utente']);
            $query->execute();
            $res = $query->get_result();
            $query->close();
        }

        $results = array();

        while ($row = $res->fetch_assoc()) {
            $row['titolo'] = stripslashes($row['titolo']);
            if (isset($row['testo'])) {
                $row['testo'] = stripslashes($row['testo']);
            }
            $results[] = $row;
        }

        if (isset($res_c)) {
            $results[1] = $res_c;
        }

        return $results;
    }

    public static function deleteData($id)
    {

        $mysqli = \DBHandle\getConnection();

        $id = intval($id);
        $id_utente = intval($_SESSION['userId']);

        $query = $mysqli->prepare('DELETE FROM articoli WHERE id = ? AND id_utente = ?');
        $query->bind_param('ii', $id, $id_utente);
        $query->execute();

        if ($query->affected_rows > 0) {
            header('Location: http://localhost/blog/miei-articoli.php?stato=ok&message=Articolo Eliminato');
            exit;
        } else {
            header('Location: http://localhost/blog/miei-articoli.php?stato=ko'
                . '&message=Ops, Non è stato possibile Eliminare l\'Articolo');
            exit;
        }
    }

    public static function updateData($form_data, $loggedInUserId)
    {
        $fields = array(
            'id' => $form_data['id'],
            'titolo' => $form_data['titolo'],
            'testo' => $form_data['testo'],
            'immagine' => $form_data['immagine'],
        );
        $fields['bozza'] = (isset($form_data['bozza']) && $form_data['bozza'] === 'on') ? 1 : 0;
        $fields['pubblicato'] = (isset($form_data['pubblicato']) && $form_data['pubblicato'] === 'on') ? 1 : 0;
        if ($fields['titolo'] === '' || $fields['testo'] === '') {
            header('Location: http://localhost/blog/insmod-articolo.php?stato=ko'
                . '&message=Titolo e Testo non possono essere lasciati vuoti');
            exit;
        }
        $fields = self::sanitize($fields);

        if ($fields) {
            $mysqli = \DBHandle\getConnection();

            $loggedInUserId = intval($loggedInUserId);
            $id = intval($fields['id']);
            try {
                $query = $mysqli->prepare('UPDATE articoli
                    SET titolo = ?, testo = ?, immagine = ?, bozza = ?, pubblicato = ?
                    WHERE id  = ? AND id_utente = ?');
                if (is_bool($query)) {
                    throw new \Exception('Query non valida. $mysqli->prepare ha restituito false.');
                }
                $img = ($fields['immagine'] !== '') ? $fields['immagine'] : null;
                $query->bind_param(
                    'sssiiii',
                    $fields['titolo'],
                    $fields['testo'],
                    $img,
                    $fields['bozza'],
                    $fields['pubblicato'],
                    $id,
                    $loggedInUserId
                );
                $query->execute();
            } catch (Exception $e) {
                error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
                header('Location: http://localhost/blog/insmod-articolo.php?id=' . $id . '&stato=ko'
                    . '&message=Non è statonpossibile aggiornare l\'articolo');
                exit;
            }

            header('Location: http://localhost/blog/insmod-articolo.php?id=' . $id . '&stato=ok'
                . '&message=Articolo Aggiornato!');
            exit;
        } else {
            header('Location: http://localhost/blog/insmod-articolo.php?stato=ko'
                . '&message=URL Immagine Non Valido&id=' . $form_data['id']);
            exit;
        }
    }
}
