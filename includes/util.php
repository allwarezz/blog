<?php
namespace DataHandling\Utils;

function get_table_head($assoc_array)
{
    $keys = array_keys($assoc_array);
    $html = '';

    foreach ($keys as $key) {
        $html .= '<th scope="col">' . ucwords($key) . '</th>';
    }
    if (!isset($_GET['id'])) {
        $html .= '<th></th>';
    }
    return $html;
}

function get_table_body($items, $extra_btn = false)
{
    $html = '';

    foreach ($items as $row) {
        $html .= '<tr>';
        foreach ($row as $key => $value) {
            if (strtolower($key) === 'dataarticolo') {
                $date_time = explode(' ', $value);
                $date = implode('/', array_reverse(explode('-', $date_time[0])));
                $html .= "<td>$date</td>";
            } elseif (strtolower($key) === 'bozza' || strtolower($key) === 'pubblicato') {
                $html .= "<td>";
                $html .= ($value) ? 'SI' : 'NO';
                $html .= "</td>";
            } else {
                $html .= "<td>$value</td>";
            }
        }

        if ($extra_btn) {
            $html .= '<td><a alt="Visualizza" title="Visualizza" href="./visualizza-articolo.php?id='
                . $row['id'] . '">📰</a>'
                . '&nbsp;&nbsp;<a alt="Modifica" title="Modifica" href="./insmod-articolo.php?id='
                . $row['id'] . '">✏</a>'
                . '&nbsp;&nbsp;<a alt="Elimina" title="Elimina" href="./includes/elimina-articolo.php?id='
                . $row['id'] . '">🗑</a></td>';
        } else {
            $html .= '<td><a alt="Visualizza" title="Visualizza" href="./visualizza-articolo.php?id='
                . $row['id'] . '">📰</a></td>';
        }
        $html .= '</tr>';
    }

    return $html;
}

function get_cards($data)
{
    $cards = '';
    foreach ($data as $value) {
        $date_time = explode(' ', $value['dataArticolo']);
        $date = implode('/', array_reverse(explode('-', $date_time[0])));
        $cards .= '<div class="card m-2" style="width: 280px;">';
        if ($value['immagine']) {
            $cards .= '<center><img src="' . $value['immagine'] . '" class="card-img-top"' .
                ' style="width: 100%; object-fit: cover; height: 240px;" alt="Immagine Articolo"></center>';
        } else {
            $cards .= '<center><img src="./assets/imgs/default.jpg" class="card-img-top"' .
                ' style="width: 100%; object-fit: cover; height: 240px;" alt="Immagine Articolo"></center>';
        }

        $cards .= '<div class="card-body border-top">';
        $cards .= '<p class="card-text" style="font-weight: bold;">' . $value['titolo'] . '</p>';
        $cards .= '<p class="card-text">Autore: <i><strong>' . strtoupper($value['autore']) . '</strong></i></p>';
        $cards .= '<p class="card-text"><small>data: <i>' . $date . '</i></small></p>';
        $cards .= '<a alt="Visualizza" title="Visualizza" class="btn btn btn-outline-dark w-100"' .
            ' href="./visualizza-articolo.php?id=' . $value['id'] . '">📰 Leggi</a>';
        $cards .= '</div>';
        $cards .= '</div>';
    }

    return $cards;
}

function show_alert($state, $message)
{
    if ($state === 'ko') {
        echo '<div class="alert alert-danger mt-3" role="alert">' . $message . '</div>';
        return;
    }

    if ($state === "ok") {
        echo '<div class="alert alert-success mt-3" role="alert">' . $message . '</div>';
        return false;
    }
}

trait InputSanitize
{
    public static function cleanInput($data)
    {
        $data = trim($data);
        $data = filter_var($data, FILTER_SANITIZE_ADD_SLASHES); //FILTER_SANITIZE_MAGIC_QUOTES
        $data = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
        return $data;
    }
}
