<?php
include_once __DIR__ . '/header.php';
include_once __DIR__ . '/DBConnection.php';
include_once __DIR__ . '/util.php';
include_once __DIR__ . '/FormHandle.php';
include_once __DIR__ . '/Articolo.php';
include_once __DIR__ . '/Commento.php';

$uri_fragments = explode("/", $_SERVER['SCRIPT_FILENAME']);

if (!isset($_SESSION['username']) && $uri_fragments[count($uri_fragments) - 1] !== 'index.php'
        && $uri_fragments[count($uri_fragments) - 1] !== 'visualizza-articolo.php'
        && $uri_fragments[count($uri_fragments) - 1] !== 'inserisci-commento.php') {
    header('Location: http://localhost/blog/login.php');
}
