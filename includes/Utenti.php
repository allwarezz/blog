<?php
namespace DataHandling;

require_once __DIR__ . '/globals.php';

use \DataHandling\Utils\InputSanitize;

class Utenti
{
    use \DataHandling\Utils\InputSanitize;
    public static function registerUser($form_data)
    {

        $fields = array(
            'username' => $form_data['username'],
            'password' => $form_data['password'],
            'password-check' => $form_data['password-check'],
        );

        $fields = self::sanitize($fields);

        if ($fields['password'] !== $fields['password-check']) {
            header('Location: http://localhost/blog/registrazione.php?stato=ko&message=Le password non corrispondono');
            exit;
        }

        $mysqli = \DBHandle\getConnection();

        $query_user = $mysqli->query("SELECT username FROM utenti WHERE username = '" . $fields['username'] . "'");

        if ($query_user->num_rows > 0) {
            header('Location: http://localhost/blog/registrazione.php?stato=ko&message=Username non disponibile');
            exit;
        }

        $query_user->close();

        $query = $mysqli->prepare('INSERT INTO utenti(username, password) VALUES (?, MD5(?))');
        $query->bind_param('ss', $fields['username'], $fields['password']);
        $query->execute();

        if ($query->affected_rows === 0) {
            error_log('Error MySQL: ' . $query->error_list[0]['error']);
            header('Location: http://localhost/blog/registrazione.php?stato=ko&message="Ops, '
                . 'Riprova più tardi c\'è stato un problema"');
            exit;
        }

        header('Location: http://localhost/blog/registrazione.php?stato=ok&message=Registrazione Avvenuta! - '
            . 'Entro 2 secondi sarai rendirizzato alla Login');
        exit;
    }

    public static function loginUser($form_data)
    {

        $fields = array(
            'username' => $_POST['username'],
            'password' => $_POST['password'],
        );

        $fields = self::sanitize($fields);

        $mysqli = \DBHandle\getConnection();

        if ($mysqli->connect_errno) {
            echo 'Connessione al database fallita: ' . $mysqli->connect_error;
            exit();
        }

        $query_user = $mysqli->query("SELECT * FROM utenti WHERE username = '" . $fields['username'] . "'");

        if ($query_user->num_rows === 0) {
            header('Location: http://localhost/blog/login.php?stato=ko&message=Utente non presente');
            exit;
        }

        $user = $query_user->fetch_assoc();

        if ($user['password'] !== md5($fields['password'])) {
            header('Location: http://localhost/blog/login.php?stato=ko&message=Password errata');
            exit;
        }

        return array(
            'id' => $user['id'],
            'username' => $user['username'],
        );
    }

    protected static function sanitize($fields)
    {
        $fields['username'] = self::cleanInput($fields['username']);

        return $fields;
    }
}
