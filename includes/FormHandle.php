<?php
namespace DataHandling;

use \DataHandling\Utils\InputSanitize;

abstract class FormHandle
{
    use \DataHandling\Utils\InputSanitize;
    abstract protected static function sanitize($fields);
    abstract public static function insertData($form_data, $loggedInUserId);
    abstract public static function selectData($args = null);
    abstract public static function deleteData($id);
    abstract public static function updateData($form_data, $loggedInUserId);
    public static function isUrlValid($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }
}
