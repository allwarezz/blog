<?php
namespace DataHandling;

class Commento extends FormHandle
{

    protected static function sanitize($fields)
    {
        $errors = array();
        $fields['testo'] = self::cleanInput($fields['testo']);
        $fields['id_articolo'] = self::cleanInput($fields['id_articolo']);

        return $fields;
    }

    public static function insertData($form_data, $loggedInUserId)
    {
        $fields = array(
            'testo' => $form_data['testo'],
            'id_articolo' => $form_data['id_articolo'],
        );
        $fields = self::sanitize($fields);
        if ($fields['testo'] === '') {
            header('Location: http://localhost/blog/visualizza-articolo.php?id=' . $fields['id_articolo'] . '&stato=ko'
                . '&message=Non si può inserire un commento vuoto');
            exit;
        }
        if ($fields) {
            $mysqli = \DBHandle\getConnection();

            $loggedInUserId = ($loggedInUserId) ? intval($loggedInUserId) : null;
            $query = $mysqli->prepare('INSERT INTO commenti(id_utente, id_articolo, testo) VALUES (?, ?, ?)');
            $query->bind_param('iis', $loggedInUserId, $fields['id_articolo'], $fields['testo']);
            $query->execute();

            if ($query->affected_rows === 0) {
                error_log('Errore MySQL: ' . $query->error_list[0]['error']);
                header('Location: http://localhost/blog/visualizza-articolo.php?id=' . $fields['id_articolo']
                    . '&stato=ko' . '&message=Ops, problema nell\'inserimento dell\'articolo');
                exit;
            }

            $query->close();

            header('Location: http://localhost/blog/visualizza-articolo.php?id=' . $fields['id_articolo'] . '&stato=ok'
                . '&message=Commento Inserito Correttamente');
            exit;
        }
    }

    public static function selectData($args = null)
    {
        $mysqli = \DBHandle\getConnection();
        $results = array();
        if (isset($args['id_articolo'])) {
            $query_c = $mysqli->prepare('SELECT commenti.testo, commenti.data_commento, utenti.username as autore
            FROM commenti LEFT JOIN utenti ON utenti.id = commenti.id_utente WHERE id_articolo = ?
            ORDER BY commenti.data_commento DESC');
            $query_c->bind_param('i', $args['id_articolo']);
            $query_c->execute();
            $res_c = $query_c->get_result();
            if (isset($res_c)) {
                while ($row = $res_c->fetch_assoc()) {
                    $row['testo'] = stripslashes($row['testo']);
                    $results[] = $row;
                }
            }
        }

        return $results;
    }

    public static function deleteData($id = null)
    {
        throw new Exception("Metodo NON Implementato");
    }

    public static function updateData($form_data = null, $id = null)
    {
        throw new Exception("Metodo NON Implementato");
    }
}
