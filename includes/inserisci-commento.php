<?php
include_once __DIR__ . '/globals.php';

$userId = null;
if (isset($_SESSION['userId'])) {
    $userId = $_SESSION['userId'];
}

\DataHandling\Commento::insertData($_POST, $userId);
