<?php
session_start();
if (isset($_SESSION['username'])) {
    header('Location: ./index.php');
    exit;
}
require_once __DIR__ . '/includes/util.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php if (isset($_GET['stato']) && $_GET['stato'] === 'ok') {
    echo '<meta http-equiv="refresh" content="2; URL=\'http://localhost/blog/login.php\'" />';
}?>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <title>Registrazione - BLOG!</title>
</head>
<body>
  <main class="container">
  <div class="border border-bottom-0 mt-5">
  <h1 class="text-center">Accedi al Blog❗</h1>
    <?php
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['message']);
}
?>
  <form method="POST" action="includes/login.php" class="container mt-3">
      <div class="col">
      <label for="username" class="form-label">Username</label>
        <input type="text" name="username" id="username" class="form-control">
      </div>
      <div class="col">
        <label for="password" class="form-label">Password</label>
        <input type="password" name="password" id="password" class="form-control">
      </div>
      <div class="col">
      <span>Non ancora registrato? </span><a href="./registrazione.php">Registrati qui</a>
      <br/>
      <span><a href="./index.php">Torna</a> all'elenco degli articoli</span>
      </div>
      <input type="submit" class="btn btn-primary mt-3" value="Accedi">
    </form>
    </div>
  </main>
</body>
</html>
