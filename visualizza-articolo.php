<?php
include_once __DIR__ . '/includes/globals.php';

$articolo = array();
if (isset($_GET['id'])) {
    $params = ['id_articolo' => $_GET['id']];
    $articolo = \DataHandling\Articolo::selectData($params);
}

?>


<main>
<?php
if (isset($_GET['stato']) && isset($_GET['message'])) {
    echo "<br/>";
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['message']);
}
?>
<?php
if (count($articolo) > 0) :
    ?>
    <?php if (isset($_SESSION['userId']) && $articolo[0]['id_utente'] == $_SESSION['userId']) : ?>
<div class="row">
    <div class="col-12 text-end mt-3"><a href="./insmod-articolo.php?id=<?php echo $articolo[0]['id']; ?>"
    class="btn btn-outline-secondary">✏ Modifica</a>
    &nbsp;<a href="./includes/elimina-articolo.php?id=<?php echo $articolo[0]['id']; ?>"
    class="btn btn-danger">🗑 Elimina</a></div>
</div>
    <?php endif;?>
<div class="card mt-3">
    <div class="card-header p-3">
        <h3><?php echo $articolo[0]['titolo']; ?></h3>
        <?php if ($articolo[0]['immagine']) : ?>
        <div><img src="<?php echo $articolo[0]['immagine']; ?>" height="60px" /></div>
        <?php endif;?>
        <small>(<?php echo $articolo[0]['autore']; ?> -
            <?php echo implode('/', array_reverse(explode('-', explode(' ', $articolo[0]['dataArticolo'])[0]))); ?>)
        </small>
    </div>
    <div class="card-body p-4">
        <p class="card-text"><?php echo $articolo[0]['testo'] ?></p>
    </div>
    <div class="card-footer text-muted p-3">
    <?php if (count($articolo[1]) === 0) : ?>
        <p><i>Non ci sono Commenti</i></p>
    <?php else : ?>
        <?php
        if (count($articolo[1]) === 1) {
            echo "<p><i>C'è 1 Commento</i></p>";
        } else {
            echo "<p><i>Ci sono " . count($articolo[1]) . " Commenti</i></p>";
        }
        ?>

        <?php foreach ($articolo[1] as $key => $value) : ?>
            <h6><?php
            $date_time = explode(' ', $value['data_commento']);
            $data = implode('/', array_reverse(explode('-', $date_time[0])));
            echo ($value['autore'] !== null) ? $value['autore'] : 'Anonimo';
            echo ' - il ' . $data . ' alle ore ' . $date_time[1];?></h6>
            <p class="border rounded p-3"><?php echo $value['testo']; ?></p>
        <?php endforeach;?>
    <?php endif;?>
    <div class="container border border-bottom-0 rounded">
        <form action="./includes/inserisci-commento.php" method="post">
        <input type="hidden" name="id_articolo" value="<?php echo $articolo[0]['id'] ?>">
        <h5 class="mt-3">
        Commenta come <?php echo (isset($_SESSION['username'])) ? $_SESSION['username'] : 'Anonimo' ?>:
        </h5>
            <textarea class="form-control" name="testo" id="testo" rows="4"></textarea>
            <input type="submit" class="btn btn-outline-primary mt-3" value="Inserisci">
        </form>
    </div>
    </div>
</div>
    <?php
else :
    echo "<div class='mt-3'>";
    \DataHandling\Utils\show_alert('ko', 'Articolo Non Trovato!');
    echo "</div>";
endif;
?>
</main>

</body>
</html>
