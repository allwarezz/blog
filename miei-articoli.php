<?php
include_once __DIR__ . '/includes/globals.php';

$articoli = \DataHandling\Articolo::selectData(['id_utente' => $_SESSION['userId']]);

if (isset($_GET['stato']) && isset($_GET['message'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['message']);
}
if (count($articoli) > 0) :
    ?>


<main>
<h4 class="mt-3">I Miei Articoli</h4>
<table class="mt-3 table table-striped table-hover table-bordered table-responsive">
  <thead>
    <?php echo \DataHandling\Utils\get_table_head($articoli[0]); ?>
  </thead>
  <tbody>
    <?php echo \DataHandling\Utils\get_table_body($articoli, true); ?>
  </tbody>
</table>
<?php else : ?>
  <p class="alert alert-dark mt-3" role="alert">Nessun articolo da mostrare.</p>
<?php endif;?>
</main>

</body>
</html>
