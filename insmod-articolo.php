<?php
include_once __DIR__ . '/includes/globals.php';

$articolo = array();
if (isset($_GET['id'])) {
    $args = ['id_articolo' => $_GET['id'], 'id_utente' => $_SESSION['userId'], 'action' => 'update'];
    $articolo = \DataHandling\Articolo::selectData($args);
    //var_dump()
}

if (isset($_GET['id']) && count($articolo) === 0) {
    \DataHandling\Utils\show_alert('ko', 'Articolo Non Trovato, Inseriscilo tu!');
}

?>


<main>
<?php if (count($articolo) > 0) : ?>
<form class="row g-3 mt-3" action="/blog/includes/modifica-articolo.php" method="post">
<input type="hidden" name="id" id="id" value="<?php echo $articolo[0]['id'] ?>">
<h2>Modifica Articolo</h2>
<?php else : ?>
  <form class="row g-3 mt-3" action="/blog/includes/inserisci-articolo.php" method="post">
<h2>Inserisci Articolo</h2>
<?php endif;?>

<span class="mt-3">
<?php
if (isset($_GET['stato']) && isset($_GET['message'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['message']);
}
?>
</span>
  <div class="col-md-6">
    <label for="titolo" class="form-label">Titolo</label>
    <input type="text" class="form-control" id="titolo" name="titolo"
      value="<?php echo (isset($articolo[0]) && $articolo[0]['titolo']) ? $articolo[0]['titolo'] :
        ((isset($_GET['titolo'])) ? $_GET['titolo'] : ''); ?>">
  </div>
  <div class="col-md-6">
    <label for="immagine" class="form-label">Link Immagine</label>
    <input type="text" class="form-control" id="immagine" name="immagine"
      value="<?php echo (isset($articolo[0]) && $articolo[0]['immagine']) ? $articolo[0]['immagine'] : ''; ?>">
  </div>
  <div class="col-12">
    <label for="testo" class="form-label">Testo Articolo</label>
    <textarea type="text" rows="5" class="form-control" id="testo" name="testo"><?php
    echo (isset($articolo[0]) && $articolo[0]['testo']) ? $articolo[0]['testo'] :
    ((isset($_GET['testo'])) ? $_GET['testo'] : ''); ?></textarea>
  </div>
  <div class="col-2">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="bozza" name="bozza"
        <?php echo (isset($articolo[0]) && $articolo[0]['bozza']) ? 'checked' : ''; ?>>
      <label class="form-check-label" for="gridCheck">
        Bozza
      </label>
    </div>
  </div>
  <div class="col-2">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="pubblicato" name="pubblicato"
      <?php echo (isset($articolo[0]) && $articolo[0]['pubblicato']) ? 'checked' : ''; ?>>
      <label class="form-check-label" for="gridCheck">
        Pubblicato
      </label>
    </div>
  </div>
  <div class="col-12">
    <button type="submit" class="btn btn-primary">
      <?php echo (count($articolo) > 0) ? 'Modifica' : 'Inserisci'; ?></button>
  </div>
</form>
</main>

</body>
</html>
