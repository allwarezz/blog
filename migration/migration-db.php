<?php
define("HOST", "localhost");
define("USERNAME", "root");
define("PASS", "");
define("DBNAME", "blog");
$conn = new mysqli(HOST, USERNAME, PASS);
// Check connection
if ($conn->connect_error) {
    die("Connessione Fallita!\n");
}

// Create database
$sql = "CREATE SCHEMA `" . DBNAME . "` DEFAULT CHARACTER SET utf8mb4 ;";
if ($conn->query($sql) === true) {
    echo "Database " . DBNAME . " Creato!\n";
    $conn->close();

    $conn = new mysqli(HOST, USERNAME, PASS, DBNAME);
    $sql = "CREATE TABLE `utenti` (
  `id` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `username` VARCHAR(50) NOT NULL UNIQUE,
  `password` VARCHAR(100) NOT NULL);";
    echo ($conn->query($sql) === true) ? "Tabella Utenti Creata!\n" : "Errore nella creazione delle Tabella Utenti!\n";
    $conn->close();

    $conn = new mysqli(HOST, USERNAME, PASS, DBNAME);
    $sql = "CREATE TABLE `articoli` (
  `id` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `id_utente` INT(11) UNSIGNED NOT NULL,
  `titolo` VARCHAR(100) NOT NULL,
  `testo` TEXT NOT NULL,
  `immagine` VARCHAR(500),
	`bozza` TINYINT(1) NOT NULL DEFAULT '0',
	`pubblicato` TINYINT(1) NOT NULL DEFAULT '0',
  `creazione` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);";
    echo ($conn->query($sql) === true) ? "Tabella Articoli Creata!\n" : "Errore nella creazione delle Tabella Articoli!\n";
    $conn->close();

    $conn = new mysqli(HOST, USERNAME, PASS, DBNAME);
    $sql = "CREATE TABLE `commenti` (
  `id` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `id_utente` INT(11) UNSIGNED,
  `id_articolo` INT(11) UNSIGNED NOT NULL,
  `testo` TEXT NOT NULL,
  `data_commento` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);";
    echo ($conn->query($sql) === true) ? "Tabella Commenti Creata!\n" : "Errore nella creazione delle Tabella Commenti!\n";
    $conn->close();

    $conn = new mysqli(HOST, USERNAME, PASS, DBNAME);
    $sql = "ALTER TABLE `articoli` ADD CONSTRAINT `articoli_id_utente_utenti_id` FOREIGN KEY (`id_utente`) REFERENCES `utenti`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION;";
    echo ($conn->query($sql) === true) ? "Vincolo articoli_id_utente_utenti_id Creato!\n" : "Errore nella creazione del vincolo articoli_id_utente_utenti_id!\n";
    $conn->close();

    $conn = new mysqli(HOST, USERNAME, PASS, DBNAME);
    $sql = "ALTER TABLE `commenti` ADD CONSTRAINT `commenti_id_utente_utenti_id` FOREIGN KEY (`id_utente`) REFERENCES `utenti`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION;";
    echo ($conn->query($sql) === true) ? "Vincolo commenti_id_utente_utenti_id Creato!\n" : "Errore nella creazione del vincolo commenti_id_utente_utenti_id!\n";
    $conn->close();

    $conn = new mysqli(HOST, USERNAME, PASS, DBNAME);
    $sql = "ALTER TABLE `commenti` ADD CONSTRAINT `commenti_id_articolo_articoli_id` FOREIGN KEY (`id_articolo`) REFERENCES `articoli`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION;";
    echo ($conn->query($sql) === true) ? "Vincolo commenti_id_articolo_articoli_id Creato!\n" : "Errore nella creazione del vincolo commenti_id_articolo_articoli_id!\n";
} else {
    echo "Errore nella creazione del DB";
}
$conn->close();
