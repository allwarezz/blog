<?php
include_once __DIR__ . '/includes/globals.php';

$articoli = \DataHandling\Articolo::selectData();

if (isset($_GET['stato']) && isset($_GET['message'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['message']);
}
if (count($articoli) > 0):
?>


<main>
<div class="container mt-4">
  <div class="row">
  <?php echo \DataHandling\Utils\get_cards($articoli); ?>
  </div>
</div>
<?php else: ?>
  <p class="alert alert-dark mt-3" role="alert">Nessun articolo da mostrare.</p>
<?php endif;?>
</main>

</body>
</html>
